from django.shortcuts import render

# import form yang ingin ditampilkan dari forms.py
from .forms import FavSubject

# Create your views here.
def example_page(request):

    # instantiate objek dictionary
    # untuk menyimpan konteks yg akan di-pass ke template
    context = {}

    # mem-pass forms ke template
    # menambahkan isi objek dictionary dgn key = from dan value = FavSubject
    context["form"] = FavSubject

    return render(request, 'example/example.html', context)