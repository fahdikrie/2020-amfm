from django import forms

class StatusForm(forms.Form):
    # TODO: add forms instance in adjacent to models' attribute
    # reference for forms fields: https://docs.djangoproject.com/en/3.1/topics/forms/#more-on-fields
    name = forms.CharField(max_length=55)
    status = forms.CharField(widget=forms.Textarea)