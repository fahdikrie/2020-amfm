from django.db import models

# Create your models here.
class Status(models.Model):
    # TODO: add models field for name & status
    # reference for models fields: https://docs.djangoproject.com/en/3.1/topics/forms/#more-on-fields
    name = models.CharField(max_length=55)
    status = models.TextField()