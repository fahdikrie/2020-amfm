from django.http import HttpResponseRedirect
from django.shortcuts import render

# Import forms & models
from .forms import StatusForm
from .models import Status

# Create your views here.
def status_page(request):

    # TODO: Implement views for status application
    # the views must consist of two (2) agendas
    # 1. pass form and statuses to template
    # 2. handle status submit request from template

    context = {}

    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)

            # parse data from raw form
            name = form.cleaned_data['name']
            status = form.cleaned_data['status']

            Status.objects.create(name=name, status=status)
            return HttpResponseRedirect('/')

    else:
        form = StatusForm()

    context["form"] = form
    context["statuses"] = Status.objects.all()

    print(context["statuses"])

    return render(request, 'status/status.html', context)